package com.example.study.components;

import com.example.study.models.School;
import com.example.study.models.User;
import com.example.study.services.SchoolService;
import com.example.study.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class DataInit {

    @Autowired
    private UserService userService;
    @Autowired
    private SchoolService schoolService;

    @PostConstruct
    public void initData() {
        initUserData();
    }

    private void initUserData() {
        User user = new User();
        user.setUsername("pavel");
        user.setPassword("123456");
        userService.createUser(user);
    }

    private void initSchoolData() {
        School school = new School();
        school.setName("TVG");
        school.setAddress("Tallinn");
        school.setPhoneNumber("3796058974");
        schoolService.createSchool(school);
    }
}
