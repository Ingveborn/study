package com.example.study.services;

import com.example.study.models.User;


public interface LoginService {
    boolean isLoginValid(User user);
}
