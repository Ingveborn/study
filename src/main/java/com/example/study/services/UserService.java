package com.example.study.services;

import com.example.study.models.User;

public interface UserService {

    void createUser(User user);

    boolean findUserByUsername(String username);
}
