package com.example.study.services.implementations;

import com.example.study.models.School;
import com.example.study.repositories.SchoolRepository;
import com.example.study.services.SchoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SchoolServiceImpl implements SchoolService {
    @Autowired
    private SchoolRepository schoolRepository;

    @Override
    public void createSchool(School school) {
        schoolRepository.save(school);
    }

    @Override
    public boolean findSchoolByName(String name) {
        return schoolRepository.findByName(name).isPresent();
    }
}
